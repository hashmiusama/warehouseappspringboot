package com.hashmiusama.warehouseapp;


import com.hashmiusama.warehouseapp.utils.DBConnection;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application {
    public static void main(String[] args) {
        // --server.port set like
        // java -jar jarfile_name --server.port=PORT_TO_START_ON
        System.err.println(DBConnection.initializeDB());
        SpringApplication.run(Application.class, args);
    }
}

