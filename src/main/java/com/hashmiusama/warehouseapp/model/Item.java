package com.hashmiusama.warehouseapp.model;

/**
 * Created by seconduser on 11/22/16.
 */
public class Item {
    private Long id;
    private String name;
    private Long customerID;
    private float weight;
    private float declaredValue;
    private Long storageID;
    private String operator;
    private Long status;

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long ID) {
        this.id = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getDeclaredValue() {
        return declaredValue;
    }

    public void setDeclaredValue(float declaredValue) {
        this.declaredValue = declaredValue;
    }

    public Long getStorageID() {
        return storageID;
    }

    public void setStorageID(Long storageID) {
        this.storageID = storageID;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Item(){

    }

    public Item(Long ID, String name, Long customerID, Long weight, Long declaredValue, Long storageID, String operator, Long status) {
        this.id = ID;
        this.name = name;
        this.customerID = customerID;
        this.weight = weight;
        this.declaredValue = declaredValue;
        this.storageID = storageID;
        this.operator = operator;
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return id.equals(item.id);
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":" + id +
                ", \"name\":\"" + name + '"' +
                ", \"customerID\":" + customerID +
                ", \"weight\":" + weight +
                ", \"declaredValue\":" + declaredValue +
                ", \"storageID\":" + storageID +
                ", \"operator\":\"" + operator + '"' +
                ", \"status\":" + status +
                '}';
    }
}
