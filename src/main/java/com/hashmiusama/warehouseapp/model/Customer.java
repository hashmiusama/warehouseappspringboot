package com.hashmiusama.warehouseapp.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Date;

/**
 * Created by seconduser on 11/22/16.
 */
public class Customer {
    private Long id;
    private String name;
    private String phoneNumber;
    private String email;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-dd-mm", locale = "pt-BR", timezone = "Brazil/East")
    private Date dateOfRegistry;

    public Customer(){

    }

    public Customer(Long ID, String name, String phoneNumber, String email, Date dateOfRegister) {
        this.id = ID;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.dateOfRegistry = dateOfRegister;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long ID) {
        this.id = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateOfRegistry() {
        return dateOfRegistry;
    }

    public void setDateOfRegistry(Date dateOfRegister) {
        this.dateOfRegistry = dateOfRegister;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":" + id +
                ", \"name\":\"" + name + '"' +
                ", \"phoneNumber\":\"" + phoneNumber + '"' +
                ", \"email\":\"" + email + '"' +
                ", \"dateOfRegistry\":\"" + dateOfRegistry + '"' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        if (!id.equals(customer.id)) return false;
        if (!phoneNumber.equals(customer.phoneNumber)) return false;
        return email.equals(customer.email);

    }

}
