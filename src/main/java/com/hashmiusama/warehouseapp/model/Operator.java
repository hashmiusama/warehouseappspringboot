package com.hashmiusama.warehouseapp.model;

public class Operator {
    String firstName;
    String lastName;
    String username;
    String password;
    boolean adminRole;

    public boolean isAdminRole() {
        return adminRole;
    }

    public void setAdminRole(boolean adminRole) {
        this.adminRole = adminRole;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Operator operator = (Operator) o;
        return operator.getUsername().equals(this.getUsername());
    }


    @Override
    public String toString() {
        return "{" +
                "\"firstname\":\"" + firstName + '"' +
                ", \"lastname\":\"" + lastName + '"' +
                ", \"username\":\"" + username + '"' +
                ", \"password\":\"" + password + '"' +
                ", \"adminRole\":" + adminRole +
                '}';
    }

    public Operator(String firstName, String lastName, String username, String password, boolean adminRole) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.adminRole = adminRole;
    }

    public Operator(){

    }

    public String getFirstName() {

        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}