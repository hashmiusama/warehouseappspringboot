package com.hashmiusama.warehouseapp.controllers;

import com.hashmiusama.warehouseapp.model.Operator;
import com.hashmiusama.warehouseapp.service.OperatorService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="/operator")
public class OperatorController {

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String operatorAdd(@RequestBody Operator operator,
                               Model model) {
        System.err.println(operator.toString());
        if(OperatorService.add(operator)){
            System.out.println("ADDED!");
            return operator.toString();
        }else{
            return "{\"error\":\"could not add\"}";
        }
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public boolean operatorUpdate(@RequestBody Operator operator,
                              Model model) {
        System.err.println(operator.toString());
        return OperatorService.update(operator);
    }

    @RequestMapping(value = "/delete/{username}", method = RequestMethod.GET)
    public boolean operatorDelete(@PathVariable String username, Model model) {
        boolean returnValue = OperatorService.delete(username);
        System.err.print(returnValue);
        return returnValue;
    }

    @RequestMapping(value = "/get/{username}", method = RequestMethod.GET)
    public String operatorGet(@PathVariable String username, Model model) {
        Operator operator = OperatorService.get(username);
        if (operator != null) {
            return operator.toString();
        }else {
            return "{\"error\":\"not found\"}";
        }
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String operatorList(Model model) {
        return OperatorService.list();
    }
}
