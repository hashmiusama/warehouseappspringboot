package com.hashmiusama.warehouseapp.controllers;

import com.hashmiusama.warehouseapp.model.Customer;
import com.hashmiusama.warehouseapp.service.CustomerService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/customer")
public class CustomerController {
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String customerAdd(@RequestBody Customer customer,
                          Model model) {
        System.err.println(customer.toString());
        if(CustomerService.add(customer)){
            System.out.println("ADDED!");
            return customer.toString();
        }else{
            return "{\"error\":\"could not add\"}";
        }
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public boolean customerUpdate(@RequestBody Customer customer,
                              Model model) {
        System.err.println(customer.toString());
        return CustomerService.update(customer);
    }

    @RequestMapping(value = "/delete/{ID}", method = RequestMethod.GET)
    public boolean customerDelete(@PathVariable Long ID, Model model) {
        return CustomerService.delete(ID);
    }

    @RequestMapping(value = "/get/{ID}", method = RequestMethod.GET)
    public String customerGet(@PathVariable Long ID, Model model) {
        Customer customer = CustomerService.get(ID);
        if (customer != null) {
            return customer.toString();
        }else {
            return "{\"error\":\"not found\"}";
        }
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String customerList(Model model) {
        return CustomerService.list();
    }
}
